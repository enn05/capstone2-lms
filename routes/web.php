<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware('can:manage')->group(function(){
// });
Route::middleware('admin')->group(function(){
    // Users Route
    Route::get('/admin/allusers', 'UserController@index');
    // Add
    Route::get('/admin/adduser', 'UserController@create');
    Route::post('/admin/adduser', 'UserController@store');
    // Edit
    Route::get('/admin/edituser/{id}', 'UserController@edit');
    Route::patch('/admin/edituser/{id}', 'UserController@update');
    // Delete
    Route::delete('/admin/deleteuser/{id}', 'UserController@destroy');

    // User Profile

    // Add
    Route::get('/admin/addprofile/{id}', 'UserProfileController@createProfile');
    Route::post('/admin/addprofile/{id}', 'UserProfileController@storeProfile');


    // Department Routes
    Route::get('/admin/alldepartments', 'DepartmentController@index');
    // Add
    Route::get('/admin/adddepartment', 'DepartmentController@create');
    Route::post('/admin/adddepartment', 'DepartmentController@store');
    // Delete
    Route::delete('/admin/deletedepartment/{id}', 'DepartmentController@destroy');
    // Edit
    Route::get('/admin/editdepartment/{id}', 'DepartmentController@edit');
    Route::patch('/admin/editdepartment/{id}', 'DepartmentController@update');

    // Position Route
    Route::get('/admin/allpositions', 'PositionController@index');
    // Add
    Route::get('/admin/addposition', 'PositionController@create');
    Route::post('/admin/addposition', 'PositionController@store');
    // Delete
    Route::delete('/admin/deleteposition/{id}', 'PositionController@destroy');
    // Edit
    Route::get('/admin/editposition/{id}', 'PositionController@edit');
    Route::patch('/admin/editposition/{id}', 'PositionController@update');
});


Route::middleware('user')->group(function(){
    // Leave Route
    Route::get('/filedleaves', 'LeaveController@index');
    // Add
    Route::get('/applyleave', 'LeaveController@create');
    Route::post('/applyleave', 'LeaveController@store');
});
