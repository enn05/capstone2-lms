<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('details');
            $table->string('date_start');
            $table->string('date_end');
            $table->string('img_path')->nullable();
            $table->unsignedBigInteger('leave_status_id');
            $table->unsignedBigInteger('leave_type_id');
            $table->unsignedBigInteger('user_profile_id');
            $table->timestamps();

            $table->foreign('leave_type_id')
            ->on('leave_types')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('leave_status_id')
            ->on('leave_statuses')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('user_profile_id')
            ->on('user_profiles')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
