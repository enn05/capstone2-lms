<?php

use Illuminate\Database\Seeder;
use App\Position;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Position::truncate();

        Position::create([
            'name' => 'CEO',
            'description' => 'Chief Executive Officer'
        ]);
        Position::create([
            'name' => 'Manager',
            'description' => 'Manages Department'
        ]);
        Position::create([
            'name' => 'Assistant Manager',
            'description' => 'Assist manager'
        ]);
        Position::create([
            'name' => 'Supervisor',
            'description' => 'Supervise department section'
        ]);
        Position::create([
            'name' => 'Assistant Supervisor',
            'description' => 'Assist supervisor'
        ]);
        Position::create([
            'name' => 'Staff',
            'description' => 'Rank and file'
        ]);
    }
}
