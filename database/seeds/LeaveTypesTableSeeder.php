<?php

use Illuminate\Database\Seeder;
use App\LeaveType;

class LeaveTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        LeaveType::truncate();

        LeaveType::create(['name' => 'Sick Leave']);
        LeaveType::create(['name' => 'Vacation Leave']);
    }
}
