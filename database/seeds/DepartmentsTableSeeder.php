<?php

use Illuminate\Database\Seeder;
use App\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Department::truncate();

        Department::create([
            'name' => 'Administration',
            'code' => 'ADM'
        ]);
        Department::create([
            'name' => 'Accounting',
            'code' => 'ACT'
        ]);
        Department::create([
            'name' => 'Customer Service Support',
            'code' => 'CSS'
        ]);
        Department::create([
            'name' => 'Marketing',
            'code' => 'MKT'
        ]);Department::create([
            'name' => 'Human Resource',
            'code' => 'HRD'
        ]);
        Department::create([
            'name' => 'Information Communication Technology',
            'code' => 'ICT'
        ]);
        Department::create([
            'name' => 'Executive Office',
            'code' => 'EOD'
        ]);
        Department::create([
            'name' => 'Logistics',
            'code' => 'LOG'
        ]);
        Department::create([
            'name' => 'Operations',
            'code' => 'OPR'
        ]);

    }
}
