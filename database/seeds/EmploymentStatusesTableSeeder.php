<?php

use Illuminate\Database\Seeder;
use App\EmploymentStatus;

class EmploymentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        EmploymentStatus::truncate();

        EmploymentStatus::create(['name' => 'Contractual']);
        EmploymentStatus::create(['name' => 'Probationary']);
        EmploymentStatus::create(['name' => 'Regular']);
        EmploymentStatus::create(['name' => 'Resigned']);
        EmploymentStatus::create(['name' => 'Terminated']);
    }
}
