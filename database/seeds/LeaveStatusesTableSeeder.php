<?php

use Illuminate\Database\Seeder;
use App\LeaveStatus;

class LeaveStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        LeaveStatus::truncate();

        LeaveStatus::create(['name' => 'Pending']);
        LeaveStatus::create(['name' => 'Approved']);
        LeaveStatus::create(['name' => 'Disapproved']);
    }
}
