<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
<div>
        <div class="parent-container">

        <table width="100%" height="100%">
        <tr>
        <td align="center" valign="middle">
        <div class="loginholder">
        <table style="background-color: white;" class="table-condensed">
        <tr>
        <td align="center">
        <h2>USER LOGIN</h2>
        </td>
        </tr>
        <tr>
        <td>
        <b>User Id:</b>
        </td>
        </tr>
        <tr>
        <td>
        <input type="text" class="inputbox" />
        </td>
        </tr>
        <tr>
        <td>
        <b>Password:</b>
        </td>
        </tr>
        <tr>
        <td>
        <input type="text" class="inputbox" />
        </td>
        </tr>
        <tr>
        <td align="center">
        <br />
        <a class="btn-normal" href="login.html">LOGIN</a>
        </td>
        </tr>
        <tr>
        <td align="left">
        <br />
        <span class="forgetpassword">Forget Password ?</span>
        </td>
        </tr>
        <tr>
        <td>
        <hr style="background-color:blue;height:1px;margin:0px;"/>

        </td>
        </tr>
        <tr>
        <td align="center">
        Login with
        </td>
        </tr>
        <tr>
        <td>
        <button class="btn-normal-logo"><span><img src="fb.png" width="34px" height="34px"/>FACEBOOK</span></button>
        &nbsp;&nbsp;
        <button class="btn-normal-logo"><span><img src="google.png" width="34px" height="34px"/>GOOGLE</span></button>
        </td>
        </tr>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
    </html>
