@extends('layouts.app')
@section('title', 'All Users')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">users</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/admin/adduser" class="btn btn-sm btn-primary">Add</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div>
                                <table class="table tablesorter" id="example">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Roles</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{ implode(', ', $user->roles()->get()->pluck('name')->toArray()) }}</td>
                                                <td class="">
                                                    <form action="/admin/deleteuser/{{$user->id}}" method="POST" class="float-right">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#deleteuser{{$user->id}}"><i class="fas fa-trash"></i></button>

                                                        <div class="modal fade" id="deleteuser{{$user->id}}">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Delete User</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Are you sure you want to delete user {{$user->name}}?</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-primary">Confirm</button>
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <a href="/admin/edituser/{{$user->id}}"><button type="button" class="float-right btn btn-link"><i class="fas fa-edit"></i></button></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$users->links()}}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
