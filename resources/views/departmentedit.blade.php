@extends('layouts.app')
@section('title', 'Edit Department')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">{{$department->name}} Department</h5>
                    </div>
                    <form action="/admin/editdepartment/{{$department->id}}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" class="form-control" value="{{$department->name}}">
                            </div>
                            <div class="form-group">
                                <label for="code">Code:</label>
                                <input type="text" name="code" class="form-control" value="{{$department->code}}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-fill btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
