@extends('layouts.app')
@section('title', 'Departments')
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">Departments</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/admin/adddepartment" class="btn btn-sm btn-primary">Add Department</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div>
                            <table class="table tablesorter table-hover" id="example">
                                <thead class="text-primary">
                                    <tr>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($departments as $department)
                                        <tr>
                                            <td>{{$department->name}}</td>
                                            <td>{{$department->code}}</td>
                                            <td>
                                                <form action="/admin/deletedepartment/{{$department->id}}" method="POST" class="float-right">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#deletedepartment{{$department->id}}"><i class="fas fa-trash"></i></button>

                                                    <div class="modal fade" id="deletedepartment{{$department->id}}">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Delete Department</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Are you sure you want to delete {{$department->name}} department?</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-primary">Confirm</button>
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </form>
                                                <a href="/admin/editdepartment/{{$department->id}}"><button type="button" class="float-right btn btn-link"><i class="fas fa-edit"></i></button></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$departments->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
