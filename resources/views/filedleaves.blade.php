@extends('layouts.app')
@section('title', 'Leaves')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">Filed Leaves</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/applyleave" class="btn btn-sm btn-primary">Apply Leave</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div>
                            <table class="table tablesorter" id="example">
                                <thead class="text-primary">
                                    <tr>
                                        <th>Date Filed</th>
                                        <th>Details</th>
                                        <th>Date Start</th>
                                        <th>Date End</th>
                                        <th>Leave Type</th>
                                        <th>Leave Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($leaves as $leave)
                                        @if($profile->id == $leave->user_profile_id)
                                            <tr>
                                                <td>{{$leave->created_at}}</td>
                                                <td>{{$leave->details}}</td>
                                                <td>{{$leave->date_start}}</td>
                                                <td>{{$leave->date_end}}</td>
                                                <td>{{$leave->leave_type->name}}</td>
                                                <td>{{$leave->leave_status->name}}</td>
                                                <td class="">
                                                    <form action="/deleteleave/{{$leave->id}}" method="POST" class="float-right">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-link"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                    <a href="/editleave/{{$leave->id}}"><button type="button" class="float-right btn btn-link"><i class="fas fa-edit"></i></button></a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
