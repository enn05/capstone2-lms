@extends('layouts.app')
@section('title', 'Add Profile')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Add Profile for {{$user->name}}</h5>
                    </div>
                    <form action="/admin/addprofile/{{$user->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{-- @method('PATCH') --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="firstName">First Name:</label>
                                        <input type="text" name="firstName" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="lastName">Last Name:</label>
                                        <input type="text" name="lastName" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="gender">Gender:</label>
                                        <select name="gender" id="" class="form-control">
                                            <option selected="">--</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other Preference</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="vacation_leave">Vacation Leave Credits:</label>
                                        <input type="number" name="vacation_leave" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="sick_leave">Sick Leave Credits:</label>
                                        <input type="number" name="sick_leave" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="employment_status_id">Employment Status:</label>
                                        <select name="employment_status_id" class="form-control">
                                                <option selected="">--</option>
                                            @foreach($employment_statuses as $employment_status)
                                                <option value="{{$employment_status->id}}">{{$employment_status->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="department_id">Department:</label>
                                        <select name="department_id" class="form-control">
                                                <option selected="">--</option>
                                            @foreach($departments as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="position_id">Position:</label>
                                        <select name="position_id" class="form-control">
                                                <option selected="">--</option>
                                            @foreach($positions as $position)
                                                <option value="{{$position->id}}">{{$position->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="img_path">Profile Image:</label>
                                <input type="file" name="img_path" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="about">About:</label>
                                <textarea name="about" id="" class="form-control"></textarea>
                            </div>
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-fill btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
