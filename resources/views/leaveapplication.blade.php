@extends('layouts.app')
@section('title', 'Leave Application')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Apply Leave</h5>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{-- @method('PATCH') --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName">Name:</label>
                                        {{-- @dump($profile) --}}
                                    <input type="text" name="firstName" class="form-control" value="{{$profile->firstName}} {{$profile->lastName}}" disabled value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName">Approver:</label>
                                        {{-- @dump($approverprofile) --}}
                                        <input type="text" name="firstName" value="{{$approver->firstName}} {{$approver->lastName}}" class="form-control" disabled value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="leave_type_id">Leave Type:</label>
                                <select name="leave_type_id" class="form-control">
                                    @foreach($leave_types as $leave_type)
                                    <option value="{{$leave_type->id}}">{{$leave_type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="date_start">From:</label>
                                        <input type="date" name="date_start" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="date_end">To:</label>
                                        <input type="date" name="date_end" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="details">Details:</label>
                                <textarea name="details" id="" class="form-control"></textarea>
                            </div>
                            {{-- @dd($profile->isd) --}}
                            <input type="hidden" name="user_profile_id" value="{{$profile->id}}">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-fill btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
