@extends('layouts.app')
@section('title', 'All Positions')
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">Positions</h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/admin/addposition" class="btn btn-sm btn-primary">Add Position</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div>
                            <table class="table tablesorter table-hover" id="example">
                                <thead class="text-primary">
                                    <tr>
                                        <th>Name</th>
                                        <th>Desciption</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($positions as $position)
                                        <tr>
                                            <td>{{$position->name}}</td>
                                            <td>{{$position->description}}</td>
                                            <td>
                                                <form action="/admin/deleteposition/{{$position->id}}" method="POST" class="float-right">
                                                    @csrf
                                                    @method('DELETE')

                                                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#deleteposition{{$position->id}}"><i class="fas fa-trash"></i></button>

                                                    <div class="modal fade" id="deleteposition{{$position->id}}">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Delete Position</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Are you sure you want to delete {{$position->name}} position?</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Confirm</button>
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <a href="/admin/editposition/{{$position->id}}">
                                                    <button type="button" class="btn btn-link float-right">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$positions->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
