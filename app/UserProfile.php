<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    //
    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function position(){
        return $this->belongsTo('\App\Position');
    }

    public function department(){
        return $this->belongsTo('\App\Department');
    }

    public function employment_status(){
        return $this->belongsTo('\App\EmploymentStatus');
    }
}
