<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $positions = Position::orderBy('name', 'asc')->paginate(5);
        return view('positions', compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('positionadd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            "name" => "required|max:30",
            "description" => "required"
        );

        $this->validate($request, $rules);

        $new_position = new Position;
        $new_position->name = $request->name;
        $new_position->description = $request->description;
        $new_position->save();

        return redirect('/admin/allpositions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $position = Position::find($id);

        return view('positionedit', compact('position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $position = Position::find($id);
        $rules = array(
            "name" => "required|max:30",
            "description" => "required"
        );

        $this->validate($request, $rules);

        $position->name = $request->name;
        $position->description = $request->description;
        $position->save();

        return redirect('/admin/allpositions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $position = Position::find($id);
        $position->delete();

        return redirect('/admin/allpositions');
    }
}
