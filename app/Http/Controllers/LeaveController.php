<?php

namespace App\Http\Controllers;

use App\Leave;
use App\LeaveStatus;
use App\LeaveType;
use App\UserProfile;
use App\User;
use Auth;
use Illuminate\Http\Request;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $leaves = Leave::all();
        $userprofile = UserProfile::where('user_id', Auth::user()->id)->get();
        foreach($userprofile as $indivprofile){
            $profile = UserProfile::find($indivprofile->id);
            return view('filedleaves', compact('leaves', 'profile'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::all();
        $leave_types = LeaveType::all();
        $userprofile = UserProfile::where('user_id', Auth::user()->id)->get();
        foreach($userprofile as $indivprofile){
            $profile = UserProfile::find($indivprofile->id);
            // dd($profile);
            // dd($approverprofile);
            $approverprofiles = UserProfile::where('department_id', $profile->department_id)->where('position_id', 2)->get();
            // dd($approverprofile);
            foreach($approverprofiles as $indivapprover){
                $approver = UserProfile::find($indivapprover->id);
                // dd($approver);
                return view('leaveapplication', compact('leave_types', 'profile', 'approver'));
            }
        }


        // dd($userprofile);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            "details" => "required",
            "date_start" => "required",
            "date_end" => "required",
            "leave_type_id" => "required",
            "img_path" => "image|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048",
            // "position" => "required",
            // "department" => "required",
            // "employement_status" => "required"
        );

        $this->validate($request, $rules);

        $new_leave = new Leave;
        $new_leave->details = $request->details;
        $new_leave->date_start = $request->date_start;
        $new_leave->date_end = $request->date_end;
        $new_leave->leave_type_id = $request->leave_type_id;
        $new_leave->leave_status_id = 1;
        $new_leave->user_profile_id = $request->user_profile_id;
        $new_leave->save();

        return redirect('/filedleaves');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function show(Leave $leave)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function edit(Leave $leave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leave $leave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leave $leave)
    {
        //
    }
}
