<?php

namespace App\Http\Controllers;

use App\UserProfile;
use App\User;
use App\Position;
use App\Department;
use App\EmploymentStatus;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function show(UserProfile $userProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(UserProfile $userProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserProfile $userProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserProfile $userProfile)
    {
        //
    }

    public function createProfile($id)
    {
        $user = User::find($id);
        $positions = Position::all();
        $departments = Department::all();
        // dd($departments);
        $employment_statuses = EmploymentStatus::all();

        return view('profileadd', compact('user', 'positions', 'departments', 'employment_statuses'));
    }

    public function storeProfile(Request $request, $id)
    {
        $user = User::find($id);
        $rules = array(
            // "fisrtName" => "required|max:30",
            // "lastName" => "required|max:30",
            // "gender" => "required",
            // "vacation_leave" => "required",
            // "sick_leave" => "required",
            "img_path" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            // "position" => "required",
            // "department" => "required",
            // "employement_status" => "required"
        );

        $this->validate($request, $rules);


        $profile = new UserProfile;
        $profile->firstName = $request->firstName;
        $profile->lastName = $request->lastName;
        $profile->gender = $request->gender;
        $profile->about = $request->about;
        $profile->vacation_leave = $request->vacation_leave;
        $profile->sick_leave = $request->sick_leave;
        $profile->user_id = $request->user_id;
        $profile->position_id = $request->position_id;
        $profile->department_id = $request->department_id;
        $profile->employment_status_id = $request->employment_status_id;

        $image = $request->file("img_path");
        $image_name = time().".".$image->getClientOriginalExtension();

        //time() gets the current time
        //getClientOriginatExtension gets file extension
        $destination = "images/"; //corresponds to /public/images

        $image->move($destination, $image_name);

        $profile->img_path = $destination.$image_name;
        // dd($profile);
        $profile->save();

        return redirect('/admin/allusers');
    }
}
