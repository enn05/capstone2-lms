<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    //
    public function leave_type(){
        return $this->belongsTo('App\LeaveType');
    }

    public function leave_status(){
        return $this->belongsTo('App\LeaveStatus');
    }
}
